"""
  A trivial web server in Python.

  Based largely on https://docs.python.org/3.4/howto/sockets.html
  This trivial implementation is not robust:  We have omitted decent
  error handling and many other things to keep the illustration as simple
  as possible.

  FIXME:
  Currently this program always serves an ascii graphic of a cat.
  Change it to serve files if they end with .html or .css, and are
  located in ./pages  (where '.' is the directory from which this
  program is run).
"""

import config    # Configure from .ini files and command line
import logging   # Better than print statements
logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.INFO)
log = logging.getLogger(__name__)
# Logging level may be overridden by configuration 

import socket    # Basic TCP/IP communication on the internet
import _thread   # Response computation runs concurrently with main program


def listen(portnum):
    """
    Create and listen to a server socket.
    Args:
       portnum: Integer in range 1024-65535; temporary use ports
           should be in range 49152-65535.
    Returns:
       A server socket, unless connection fails (e.g., because
       the port is already in use).
    """
    # Internet, streaming socket
    serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # Bind to port and make accessible from anywhere that has our IP address
    serversocket.bind(('', portnum))
    serversocket.listen(1)    # A real server would have multiple listeners
    return serversocket


def serve(sock, func):
    """
    Respond to connections on sock.
    Args:
       sock:  A server socket, already listening on some port.
       func:  a function that takes a client socket and does something with it
    Returns: nothing
    Effects:
        For each connection, func is called on a client socket connected
        to the connected client, running concurrently in its own thread.
    """
    while True:
        log.info("Attempting to accept a connection on {}".format(sock))
        (clientsocket, address) = sock.accept()
        _thread.start_new_thread(func, (clientsocket,))


##
# Starter version only serves cat pictures. In fact, only a
# particular cat picture.  This one.
##

#Cat variable used as placeholder, will not appear in browser at all when this is finished
CAT = "This is supposed to be a picture of a cat."

#figures out what the location of the file is supposed to be

def fileFinder(data):

  i = 0 #counter for the while loop
  x = 5 #this should start the search for the file name after _GET_
  
  while i == 0:
    if data[x] != " ":
      x += 1
    else:
      i = 1
    
  str1 = "pages"
  str2 = str1 + data[4:x]
  return str2

#Checks if location is forbidden

def isForbidden(location):
  if "~" in location:
    return STATUS_FORBIDDEN
  if "//" in location:
    return STATUS_FORBIDDEN
  if ".." in location:
    return STATUS_FORBIDDEN
  else:
    return location
  
#checks if the file does actually exist in the pages folder  

def doesExist(location):
  if location == STATUS_FORBIDDEN:
    return location
  try:
    file = open(location, "r")
  except:
    location = STATUS_NOT_FOUND
  else:
    file = location
  return location

# HTTP response codes, as the strings we will actually send.
# See:  https://en.wikipedia.org/wiki/List_of_HTTP_status_codes
# or    http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
##
STATUS_OK = "HTTP/1.0 200 OK\n\n"
STATUS_FORBIDDEN = "HTTP/1.0 403 Forbidden\n\n"
STATUS_NOT_FOUND = "HTTP/1.0 404 Not Found\n\n"
STATUS_NOT_IMPLEMENTED = "HTTP/1.0 401 Not Implemented\n\n"

def respond(sock):
    """
    This server responds only to GET requests (not PUT, POST, or UPDATE).
    Any valid GET request is answered with an ascii graphic of a cat.
    """
    sent = 0
    request = sock.recv(1024)  # We accept only short requests
    request = str(request, encoding='utf-8', errors='strict')
    log.info("--- Received request ----")
    log.info("Request was {}\n***\n".format(request))
    
#function to find the file we are looking for
    file = fileFinder(str(format(request)))
  
#function to check for a 403
    if isForbidden(file) == STATUS_FORBIDDEN:
      file = STATUS_FORBIDDEN
      print("file is forbidden!")
    else:
      print("file was not forbidden")
    
  
#check if location exists
    if doesExist(file) == STATUS_NOT_FOUND:
      page = STATUS_NOT_FOUND
      print("File not found, transmitting 404")
    if file == STATUS_FORBIDDEN:
      page = STATUS_FORBIDDEN
      print("transmitting 403")
      transmit(STATUS_FORBIDDEN, sock)
      sock.shutdown(socket.SHUT_RDWR)
      sock.close()
      return
    else:
#and if we find the file and its not forbidden, we will transmit it
      try:
        page = open(file, "r", encoding="utf-8")

        
#this will transmit the file, assuming that it isn't forbidden or not found
        parts = request.split()
        if len(parts) > 1 and parts[0] == "GET":
            transmit(STATUS_OK, sock)
        
            with open(file, "r", encoding="utf-8") as page:
              for line in page:
                transmit(line.strip(), sock)
        else:
            log.info("Unhandled request: {}".format(request))
            transmit(STATUS_NOT_IMPLEMENTED, sock)
            transmit("\nI don't handle this request: {}\n".format(request), sock)
      except:
        page = STATUS_NOT_FOUND
        transmit(page, sock)
      
      sock.shutdown(socket.SHUT_RDWR)
      sock.close()
    return


def transmit(msg, sock):
    """It might take several sends to get the whole message out"""
    sent = 0
    while sent < len(msg):
        buff = bytes(msg[sent:], encoding="utf-8")
        sent += sock.send(buff)

###
#
# Run from command line
#
###


def get_options():
    """
    Options from command line or configuration file.
    Returns namespace object with option value for port
    """
    # Defaults from configuration files;
    #   on conflict, the last value read has precedence
    options = config.configuration()
    # We want: PORT, DOCROOT, possibly LOGGING

    if options.PORT <= 1000:
        log.warning(("Port {} selected. " +
                         " Ports 0..1000 are reserved \n" +
                         "by the operating system").format(options.port))

    return options


def main():
    options = get_options()
    port = options.PORT
    if options.DEBUG:
        log.setLevel(logging.DEBUG)
    sock = listen(port)
    log.info("Listening on port {}".format(port))
    log.info("Socket is {}".format(sock))
    serve(sock, respond)


if __name__ == "__main__":
    main()
